﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static event Action OnWalkEnable;
    public static event Action OnWalkDisable;
    public static PlayerController instance;
    public static bool IsTake { get { return _IsTake; } }
    public static bool IsMoving { get { return _IsMoving; } }
    private int _WalkRemamber;
    private int _MaxWalkRemember = 3;

    private static bool _IsMoving = false;
    private static bool _IsTake = false;

    private Animator anim;
    private Coroutine cor;

    private void Awake()
    {
        instance = this;
    }
    private void OnEnable()
    {
        _WalkRemamber = 0;
        _IsTake = false;
        InputBase.OnHandUp += Take;
        InputBase.OnLeftHandUp += Left;
        InputBase.OnRightHandUp += Rigth;
        InputBase.OnJump += Walk;
        InputBase.OnTwoHandsUp += TPose;
        InputBase.OnTwoHandsDown += Idle;
    }
    private void OnDisable()
    {
        InputBase.OnHandUp -= Take;
        InputBase.OnLeftHandUp -= Left;
        InputBase.OnRightHandUp -= Rigth;
        InputBase.OnJump -= Walk;
        InputBase.OnTwoHandsUp -= TPose;
        InputBase.OnTwoHandsDown -= Idle;
    }

    private void GameController_OnLevel2()
    {
        Idle();
    }

    private void Start()
    {
        _IsMoving = false;
        anim = GetComponent<Animator>();
    }
    [ContextMenu("+++Walk")]
    private void Walk()
    {
        if (GameController.St == GameController.State.Run)
        {
            if (_WalkRemamber <= 0)
            {
                anim.SetInteger("TPose", 0);
                anim.SetBool("Walk", true);
                _IsMoving = true;
                if (OnWalkEnable != null)
                    OnWalkEnable();
                _WalkRemamber = 1;
                StartCoroutine(GetAnimaionWalkState());
            }
            else if(_WalkRemamber < _MaxWalkRemember)
            {
                _WalkRemamber++;
            }
        }
    }
    [ContextMenu("+++GoLeft")]
    private void Left()
    {
        if (GameController.St == GameController.State.Run)
        {
            if(transform.position.x > -0.5f)
                anim.SetTrigger("GoLeft"); 
        }
        else
        if(GameController.St == GameController.State.Level2)
        {
            anim.SetInteger("TPose", 3);
        }
    }
    [ContextMenu("+++GoRight")]
    private void Rigth()
    {
        if (GameController.St == GameController.State.Run)
        {
            if(transform.position.x < 0.5f)
            {
                print(transform.position.x);
                anim.SetTrigger("GoRight");
            }
        }
        else if(GameController.St == GameController.State.Level2)
        {
            anim.SetInteger("TPose", 2);
        }
    }
    [ContextMenu("+++Take")]
    private void Take()
    {
        if(GameController.St == GameController.State.Run)
        {
            _IsTake = true;
            cor = StartCoroutine(GetAnimaionTakeState());
            anim.SetTrigger("Take");
        }
    }

    [ContextMenu("+++Idle")]
    private void Idle()
    {
        anim.SetInteger("TPose", 0);
        //anim.SetBool("Walk", false);
        //anim.Play("Idle");
    }
    [ContextMenu("+++TPose")]
    private void TPose()
    {
        if(GameController.St == GameController.State.Level2)
        {
            anim.SetBool("Walk", false);
            anim.SetTrigger("Idle");
            anim.SetInteger("TPose", 1);
        }
    }
    [ContextMenu("+++LeftHand")]
    private void LeftHand()
    {
        anim.SetInteger("TPose", 3);
    }
    [ContextMenu("+++RightHand")]
    private void RightHand()
    {
        anim.SetInteger("TPose", 2);
    }


    private IEnumerator GetAnimaionTakeState()
    {
        while (true)
        {
            yield return new WaitForSeconds(2f);
            _IsTake = false;
            StopCoroutine(cor);
        }
    }
    private IEnumerator GetAnimaionWalkState()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            if (_WalkRemamber <= 0)
            {
                //print("WalkRemamberExit   " + WalkRemamber);
                anim.SetBool("Walk", false);
                _IsMoving = false;
                if (OnWalkDisable != null)
                    OnWalkDisable();
                StopAllCoroutines();
                //yield break;
            }
            _WalkRemamber--;
        }
    }
}

