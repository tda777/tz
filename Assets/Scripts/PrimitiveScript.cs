﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrimitiveScript : MonoBehaviour
{

    public static event System.Action<PrimitiveScript> OnCollisionBarrier;
    public static event System.Action<PrimitiveScript> OnCollisionBunus;
    public enum TypePrimitive { Bonus , Barrier};
    [SerializeField] private TypePrimitive _Type;
    [SerializeField] private List<Material> _Mat = new List<Material>();

    private void Reset()
    {
        _Type = TypePrimitive.Barrier;
    }
    void Start ()
    {
        if (_Mat.Count == 0)
        {
            _Mat.AddRange(GameController.instance.Mat);
        }
        transform.parent.GetComponent<MoveChunk>().Register(this);
        if (_Type == TypePrimitive.Barrier) GetComponent<Renderer>().material = _Mat[0];
        else GetComponent<Renderer>().material = _Mat[1];
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other != null)
        {
            if (other.gameObject.tag == PlayerController.instance.gameObject.tag)
                if (_Type == TypePrimitive.Barrier)
                {
                    if (OnCollisionBarrier != null)
                        OnCollisionBarrier(this);
                }
                else
                {
                    if (OnCollisionBunus != null)
                        OnCollisionBunus(this);
                }
        }
    }
}
