﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Windows.Kinect;

public class InputKinect : InputBase
{


    [SerializeField] private GameObject m_BodySourceManager;

    private BodySourceManager _BodyManager;
    private Body[] _Bodies;
    private float _SholPos;
    private float _HandLPos;
    private float _HandRPos;
    private float _HeadPos;
    private float _SpinePos;
    private object _HandLeftIsUp;
    private object _HandRightIsUp;
    private object _HandUnderHead;
    private object _TwoHandIsUp;
    private object _IdleWaiting;
    private object _JumpIsUp;
    private PointSpeed _HandLSpeed;
    private PointSpeed _HandRSpeed;
    private PointSpeed _SpineSpeed;
    private bool _IsTracked;
    private float _MinSpeedHand = 15f;
    private float _MinSpeedSpine = 1f;
    private float _SpinePosMem;
    private float _DirOnJump;
    


    #region Remuve IT!!!!
    public UnityEngine.UI.Text txt1;
    public UnityEngine.UI.Text txt2;
    #endregion

    void Start ()
    {
        _BodyManager = m_BodySourceManager.GetComponent<BodySourceManager>();
        float time = Time.time;
        _HandLSpeed = new PointSpeed(5, time);
        _HandRSpeed = new PointSpeed(5, time);
        _SpineSpeed = new PointSpeed(5, time);
        _SpinePosMem = 0;
    }
	
	void Update ()
    {
        if (_BodyManager == null) return;
        _Bodies = _BodyManager.GetData();
        if (_Bodies == null) return;
        txt2.text = "KINECT off"; txt2.color = Color.red;
        _SholPos = 0;
        _HandLPos = 0;
        _HandRPos = 0;
        _HeadPos = 0;
        _SpinePos = 0;
        foreach (var body in _Bodies)
        {
            if (body == null) continue;

            _IsTracked = body.IsTracked;
            if (_IsTracked)
            {
                txt2.text = "KINECT IsTracked"; txt2.color = Color.green;
                if (body.Joints.ContainsKey(JointType.SpineShoulder))
                    _SholPos = body.Joints[JointType.SpineShoulder].Position.Y;
                if (body.Joints.ContainsKey(JointType.WristLeft))
                    _HandLPos = body.Joints[JointType.WristLeft].Position.Y;
                if (body.Joints.ContainsKey(JointType.WristRight))
                    _HandRPos = body.Joints[JointType.WristRight].Position.Y;
                if (body.Joints.ContainsKey(JointType.Head))
                    _HeadPos = body.Joints[JointType.Head].Position.Y;
                if (body.Joints.ContainsKey(JointType.SpineMid))
                    _SpinePos = body.Joints[JointType.SpineMid].Position.Y;


            }
        }

        txt1.text = "Hand Left :" + _HandLPos +
            "\n\rHand Right:" + _HandRPos +
            "\n\rSpine     :" + _SpinePos;
        UpdSpeeds();
        CheckHandUp();
        Jumping();
    }

    private void Jumping()
    {
        _DirOnJump = _SpinePos - _SpinePosMem;
        _SpinePosMem = _SpinePos;

        if (_SpineSpeed.Speed > _MinSpeedSpine && _DirOnJump > 0)
        {
            if(_JumpIsUp == null)
            {
                _JumpIsUp = new object();
                Jump();
                StartCoroutine(JumpIsUp());
            }
        }
    }

    private void UpdSpeeds()
    {
        float time = Time.time;
        if (_HandLPos != 0) _HandLSpeed.AddPosition(_HandLPos, time);
        if (_HandRPos != 0) _HandRSpeed.AddPosition(_HandRPos, time);
        if (_SpinePos != 0) _SpineSpeed.AddPosition(_SpinePos, time);

        #region Remuve IT!!!!
        //_HandLSpeed.AddPosition(test1.transform.position.y, time);
        txt1.text += "\n\rHand Left  Vel:" + (_HandLSpeed.Speed).ToString("0#.####")
        + "\n\rHand Right Vel:" + (_HandRSpeed.Speed).ToString("0#.####")
        + "\n\rSpine     Vel:" + (_SpineSpeed.Speed).ToString("0#.####");
        #endregion
    }

    // если рука поднялась выше торса вызываем событие влево
    private void CheckHandUp()
    {
        if (_HandLPos < _HeadPos && _HandRPos < _HeadPos)
        {// руки ниже головы
            if (_HandLPos > _SpinePos && _HandRPos < _SpinePos)
            {// левая вверху правая внизу
                if (_HandLeftIsUp == null && _HandLSpeed.Speed < _MinSpeedHand)
                {
                    _HandLeftIsUp = new object();
                    LeftHandUp();
                    StartCoroutine(HandLeftUpping());
                }
            }
            else if (_HandRPos > _SpinePos && _HandLPos < _SpinePos)
            {// правая вверху левая внизу
                if (_HandRightIsUp == null && _HandRSpeed.Speed < _MinSpeedHand)
                {
                    _HandRightIsUp = new object();
                    RightHandUp();
                    StartCoroutine(HandRightUpping());
                }
            }else if(_HandRPos > _SpinePos && _HandLPos > _SpinePos)
            {//обе руки выше спины
                if (_TwoHandIsUp == null )
                {
                    _TwoHandIsUp = new object();
                    TwoHandsUp();
                    StartCoroutine(TwoHandsUpping());
                }
            }else if((_HandRPos > _SpinePos || _HandLPos > _SpinePos) && 
                _HandLSpeed.Speed < _MinSpeedHand && _HandRSpeed.Speed < _MinSpeedHand)
            {//любая из рук поднята над поясом
                if(_IdleWaiting == null)
                {
                    _IdleWaiting = new object();
                    StartCoroutine(IdleWaiting());
                }
            }

        }else if(_HandLPos > _HeadPos || _HandRPos > _HeadPos)
        {// любая рука выше головы
            if (_HandUnderHead == null&& _HandRightIsUp == null&& _HandRightIsUp == null)
            {
                _HandUnderHead = new object();
                HandUp();
                StartCoroutine(HandUnderHead());
            }
        }
    }


    private IEnumerator JumpIsUp()
    {
        while (true)
        {
            if (_SpinePos != 0)
            {
                if (_DirOnJump < 0)
                {
                    _JumpIsUp = null;
                    yield break;
                }
            }
            yield return null;
        }
    }

    // корутина обнуляет объект-блокировщик если обе руки опущены ниже пояса и запускает событие две руки опущены
    private IEnumerator IdleWaiting()
    {
        while (true)
        {
            if (_HandLPos != 0 &&
                _HandRPos != 0 &&
                _SpinePos != 0)
            {
                if (_HandLPos < _SpinePos &&
                    _HandRPos < _SpinePos)
                {
                    TwoHandsDown();
                    _IdleWaiting = null;
                    yield break;
                }
            }
            yield return null;
        }
    }

    // корутина обнуляет объект-блокировщик если обе руки опущены ниже пояса и запускает событие две руки опущены
    private IEnumerator TwoHandsUpping()
    {
        while (true)
        {
            if (_HandLPos != 0 &&
                _HandRPos != 0 &&
                _SpinePos != 0)
            {
                if (_HandLPos < _SpinePos &&
                    _HandRPos < _SpinePos)
                {
                    TwoHandsDown();
                    _TwoHandIsUp = null;
                    yield break;
                }
            }
            yield return null;
        }
    }

    // корутина обнуляет объект-блокировщик если обе руки ниже головы
    private IEnumerator HandUnderHead()
    {
        while (true)
        {
            if (_HandLPos != 0 &&
                _HandRPos != 0 &&
                _HeadPos != 0)
            {
                if (_HandLPos < _HandRPos &&
                    _HandRPos < _HeadPos)
                {

                    _HandUnderHead = null;
                    yield break;
                }
            }
            yield return null;
        }
    }

    // корутина обнуляет объект-блокировщик если рука опутилась ниже торса или выше головы
    private IEnumerator HandLeftUpping()
    {
        while (true)
        {
            if (_HandLPos != 0 &&
                _SpinePos != 0 &&
                _HeadPos != 0)
            {
                if (_HandLPos < _SpinePos ||
                    _HandLPos > _HeadPos)
                {
                    TwoHandsDown();
                    _HandLeftIsUp = null;
                    yield break;
                }
            }
            yield return null;
        }
    }

    // корутина обнуляет объект-блокировщик если рука опутилась ниже торса
    private IEnumerator HandRightUpping()
    {
        while (true)
        {
            if (_HandRPos != 0 &&
                _SpinePos != 0 &&
                _HeadPos != 0)
            {
                if (_HandRPos < _SpinePos ||
                    _HandLPos > _HeadPos)
                {
                    TwoHandsDown();
                    _HandRightIsUp = null;
                    yield break;
                }
            }
            yield return null;
        }
    }

    
}
